/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nmea_message_callback_napi.h"

#include "common_utils.h"
#include "ipc_skeleton.h"
#include "location_log.h"
#include "napi_util.h"
#include "constant_definition.h"
#include "napi/native_api.h"
#include "napi/native_common.h"

namespace OHOS {
namespace Location {
NmeaMessageCallbackNapi::NmeaMessageCallbackNapi()
{
    env_ = nullptr;
    handlerCb_ = nullptr;
    remoteDied_ = false;
}

NmeaMessageCallbackNapi::~NmeaMessageCallbackNapi()
{
}

int NmeaMessageCallbackNapi::OnRemoteRequest(
    uint32_t code, MessageParcel& data, MessageParcel& reply, MessageOption& option)
{
    LBSLOGD(NMEA_MESSAGE_CALLBACK, "NmeaMessageCallbackNapi::OnRemoteRequest!");
    if (data.ReadInterfaceToken() != GetDescriptor()) {
        LBSLOGE(NMEA_MESSAGE_CALLBACK, "invalid token.");
        return -1;
    }
    if (remoteDied_) {
        LBSLOGD(NMEA_MESSAGE_CALLBACK, "Failed to `%{public}s`,Remote service is died!", __func__);
        return -1;
    }

    switch (code) {
        case RECEIVE_NMEA_MESSAGE_EVENT: {
            int64_t timestamp = data.ReadInt64();
            std::string msg = Str16ToStr8(data.ReadString16());
            OnMessageChange(timestamp, msg);
            break;
        }
        default: {
            IPCObjectStub::OnRemoteRequest(code, data, reply, option);
            break;
        }
    }
    return 0;
}

bool NmeaMessageCallbackNapi::IsRemoteDied()
{
    return remoteDied_;
}

napi_value NmeaMessageCallbackNapi::PackResult(const std::string msg)
{
    napi_value result;
    NAPI_CALL(env_, napi_create_string_utf8(env_, msg.c_str(), NAPI_AUTO_LENGTH, &result));
    return result;
}

bool NmeaMessageCallbackNapi::Send(const std::string msg)
{
    std::unique_lock<std::mutex> guard(mutex_);
    uv_loop_s *loop = nullptr;
    NAPI_CALL_BASE(env_, napi_get_uv_event_loop(env_, &loop), false);
    if (loop == nullptr) {
        LBSLOGE(NMEA_MESSAGE_CALLBACK, "loop == nullptr.");
        return false;
    }
    if (handlerCb_ == nullptr) {
        LBSLOGE(NMEA_MESSAGE_CALLBACK, "handler is nullptr.");
        return false;
    }
    uv_work_t *work = new (std::nothrow) uv_work_t;
    if (work == nullptr) {
        LBSLOGE(NMEA_MESSAGE_CALLBACK, "work == nullptr.");
        return false;
    }
    NmeaAsyncContext *context = new (std::nothrow) NmeaAsyncContext(env_);
    if (context == nullptr) {
        LBSLOGE(NMEA_MESSAGE_CALLBACK, "context == nullptr.");
        delete work;
        return false;
    }
    if (!InitContext(context)) {
        LBSLOGE(NMEA_MESSAGE_CALLBACK, "InitContext fail");
        return false;
    }
    context->msg = msg;
    work->data = context;
    UvQueueWork(loop, work);
    return true;
}


void NmeaMessageCallbackNapi::UvQueueWork(uv_loop_s* loop, uv_work_t* work)
{
    uv_queue_work(
        loop,
        work,
        [](uv_work_t *work) {},
        [](uv_work_t *work, int status) {
            NmeaAsyncContext *context = nullptr;
            napi_handle_scope scope = nullptr;
            if (work == nullptr) {
                LBSLOGE(LOCATOR_CALLBACK, "work is nullptr!");
                return;
            }
            context = static_cast<NmeaAsyncContext *>(work->data);
            if (context == nullptr || context->env == nullptr) {
                LBSLOGE(LOCATOR_CALLBACK, "context is nullptr!");
                delete work;
                return;
            }
            NAPI_CALL_RETURN_VOID(context->env, napi_open_handle_scope(context->env, &scope));
            if (scope == nullptr) {
                LBSLOGE(NMEA_MESSAGE_CALLBACK, "scope is nullptr");
                delete context;
                delete work;
                return;
            }
            napi_value jsEvent;
            CHK_NAPI_ERR_CLOSE_SCOPE(context->env,
                napi_create_string_utf8(context->env, context->msg.c_str(), NAPI_AUTO_LENGTH, &jsEvent),
                scope, context, work);
            if (context->callback[0] != nullptr) {
                napi_value undefine;
                napi_value handler = nullptr;
                CHK_NAPI_ERR_CLOSE_SCOPE(context->env, napi_get_undefined(context->env, &undefine),
                    scope, context, work);
                CHK_NAPI_ERR_CLOSE_SCOPE(context->env,
                    napi_get_reference_value(context->env, context->callback[0], &handler),
                    scope, context, work);
                if (napi_call_function(context->env, nullptr, handler, 1, &jsEvent, &undefine) != napi_ok) {
                    LBSLOGE(NMEA_MESSAGE_CALLBACK, "Report event failed");
                }
            }
            NAPI_CALL_RETURN_VOID(context->env, napi_close_handle_scope(context->env, scope));
            uint32_t refCount = INVALID_REF_COUNT;
            napi_reference_unref(context->env, context->callback[0], &refCount);
            if (refCount == 0) {
                NAPI_CALL_RETURN_VOID(context->env, napi_delete_reference(context->env, context->callback[0]));
            }
            delete context;
            delete work;
    });
}

void NmeaMessageCallbackNapi::OnMessageChange(int64_t timestamp, const std::string msg)
{
    LBSLOGD(NMEA_MESSAGE_CALLBACK, "NmeaMessageCallbackNapi::OnMessageChange");
    Send(msg);
}

void NmeaMessageCallbackNapi::DeleteHandler()
{
    std::unique_lock<std::mutex> guard(mutex_);
    if (handlerCb_ == nullptr || env_ == nullptr) {
        LBSLOGE(NMEA_MESSAGE_CALLBACK, "handler or env is nullptr.");
        return;
    }
    uint32_t refCount = INVALID_REF_COUNT;
    napi_reference_unref(env_, handlerCb_, &refCount);
    if (refCount == 0) {
        NAPI_CALL_RETURN_VOID(env_, napi_delete_reference(env_, handlerCb_));
        handlerCb_ = nullptr;
    }
}
}  // namespace Location
}  // namespace OHOS
